<?php
require_once __DIR__ . '/vendor/autoload.php';

class App extends Sunra\PhpSimple\HtmlDomParser
{
    const PROXY_LIST    = __DIR__ . '/proxies.txt';
    const CSV_FOLDER    = __DIR__ . '/csv/';
    const GOOGLE_SEARCH = 'http://google.com/search?q=site:';
    const USE_PROXY     = false; // for debugging
    
    private $results = [];
    private $downloadLink = '';
    
    public function __construct(){
        $this->formAction();
    }
    
    /*
    * Give success message
    * @return json
    */
    private function success($result) {
        header("HTTP/1.0 200");
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode($result));
    }
    
    /*
    * Give error message
    * @return string
    */
    private function error($errorCode = 400, $msg = 'Bad request!'){
        header("HTTP/1.0 {$errorCode} {$msg}");
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode($msg));
    }
    
    /*
    * Trim urls from form and parse them into an array
    * @param string urls comma seperated
    * @return array
    */
    private function getUrls($urls) {
       if (trim($urls) == '') {
            return $this->error(400, 'Urls is empty!');
        }
        
        return explode(',', $urls); 
    }
    
    /*
    * Sanitzate the url and check if the url is valid
    * @param string url
    * @return boolean on false
    * @return string valid url
    */
    private function validate($url) {
        // Sanitize
        $url = filter_var($url, FILTER_SANITIZE_URL);
        
        // Validate
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }
        
        return $url;
    }
    
    /*
    * Set an proxy
    * @param string proxy
    * @return string
    */
    private function setProxy($proxy) {
        $context['http'] = ['proxy' => '193.178.229.250:44268', 'request_fulluri' => true, 'follow_location' => 1];
        $stream = stream_context_create($context);
        return $stream;
    }
    
    /*
    * Get the html results
    * @param string url
    * @return boolean false on failed request
    * @return string html content
    */
    private function getHtml($url) {
        $url = Self::GOOGLE_SEARCH . urlencode($url);
        
        if (!Self::USE_PROXY) {
            $html = $this->file_get_html($url);
        } else {
            $proxies    = file(Self::PROXY_LIST, FILE_IGNORE_NEW_LINES);
            $proxy      = $proxies[ array_rand($proxies)];
            $stream     = $this->setProxy($proxy);
            $html       = $this->file_get_html($url, false, $stream);
        }
        
        if (!$html) {
            return $this->error(503, 'Service Unavailable!');
        }
        
        return $html;
    }
    
    private function getHtmlCurl($url) {
        //http://www.webscrapingblog.com/how-to-use-curl-with-php-simple-html-dom-for-data-scraping/
        //https://github.com/clarketm/proxy-list/blob/master/proxy-list.txt
        $agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1';
        $proxies    = file(Self::PROXY_LIST, FILE_IGNORE_NEW_LINES);
        $proxy      = $proxies[ array_rand($proxies)];
        
        $url = Self::GOOGLE_SEARCH . urlencode($url);
 
        // Some websites require referrer
        $host = parse_url($url, PHP_URL_HOST);
        $scheme = parse_url($url, PHP_URL_SCHEME);
        $referrer = $scheme . '://' . $host; 
        $curl = curl_init();
     
    	curl_setopt($curl, CURLOPT_HEADER, false);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_PROXY, $proxy);
        curl_setopt($curl, CURLOPT_USERAGENT, $agent);
        curl_setopt($curl, CURLOPT_REFERER, $referrer);
    	
        curl_setopt($curl, CURLOPT_COOKIESESSION, 0);
        curl_setopt($curl, CURLOPT_COOKIEFILE, COOKIE_FILENAME);
        curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIE_FILENAME);
     
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
     
        // allow to crawl https webpages
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
     
        // the download speed must be at least 1 byte per second
        curl_setopt($curl,CURLOPT_LOW_SPEED_LIMIT, 1);
     
        // if the download speed is below 1 byte per second for more than 30 seconds curl will give up
        curl_setopt($curl,CURLOPT_LOW_SPEED_TIME, 30);
     
        $content = curl_exec($curl);
        return $this->str_get_html($content);
    }
    
    /*
    * Check if the url is indexed
    * @param string url
    * @return boolean
    */
    private function isIndexed($url) {
        $html = $this->getHtml($url);
        
        
        // Check if is indexed
        foreach($html->find('div#resultStats') as $el) {
           if ($el->plaintext) return true;
        }
        
        return false;
    }
    
    /*
    * Save result in csv
    * @return void
    */
    private function saveResult() {
        $filename = time() . date('_d-m-Y') . '.csv';
        $fields = [];
        $fp = fopen(Self::CSV_FOLDER . $filename, 'w');
        
        fputcsv($fp, ['url', 'indexed']);
        
        foreach ($this->results as $result) {
            $field = [$result['url'], ($result['indexed']) ? 'true' : 'false'];
            fputcsv($fp, $field);
        }

        $this->downloadLink = "csv/{$filename}";
        fclose($fp);
    }
    
    private function getResult($urls) {
        
        foreach($urls as $url) {
            $sanitizedUrl = $this->validate($url);
            
            if (!$sanitizedUrl) {
                return $this->error(400, "{$url} is not a valid url!");
            }
            
            $isIndexed = $this->isIndexed($sanitizedUrl);
            $this->results[] = ['url' => $sanitizedUrl, 'indexed' => $isIndexed];
        }
        
        $this->saveResult();
        
        return $this->success([
            'result' => $this->results, 
            'download_link' => $this->downloadLink
        ]);
    }
    
    /*
    * Handles the form submit
    * @return void
    */
    public function formAction() {
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['urls'])) {
            $urls = $this->getUrls($_POST['urls']);
            $result = $this->getResult($urls);
            
            return $result;
        }
    }
}

$app = new App();