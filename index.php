<?php require_once('./gi.php'); ?>

<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Google Index Checker</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        .container { margin-top: 3rem;}
        table { margin: 1rem 0; padding: 1rem;}
        textarea {
            border: none;
            background-color: #f0eeee;
            padding: 1rem;
            margin: 1rem 0;
        }
    </style>
</head>
<body>
    
    <div class="container">
        <div class='alert' role='alert'></div>
        <h3>Google Index Checker</h3>
            
        <div class="row">
            <div class="col-sm tab-content">
                <textarea class='urls-input' rows="20" cols="50" placeholder="Urls must be comma seperated!"></textarea>
                <button class="submit btn btn-info">Check</button>
                <span class='count-result'></span>
            </div>
            <div class="col-sm">
                <table class="table">
                  <thead class="thead">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Url</th>
                      <th scope="col">Indexed</th>
                    </tr>
                  </thead>
                  <tbody class='result'></tbody>
                </table>
            </div>
        </div>
    </div>

</body>
</html>

<script>
$('.submit').on('click', function(){
    var urls = $('.urls-input').val();
    
    $('.alert').removeClass('alert-danger');
    $('.alert').removeClass('alert-success');
    $('.alert').text('Loading...');

    $.ajax({
        type: 'POST',
        url: './gi.php',
        data: {urls: urls},
        success: function (data) {
            $('.alert').text('');
            $('.alert').addClass('alert-success');
            $('.result').empty();
            
            // Download link
            $('.alert-success').append(
                `Submission was successful.
                <a href="${data.download_link}">Download link</a>
                `
            );
            var count = data.result.length
            var id = 1;
            
            $('.count-result').text(`There're ${count} results.`);
            $.each(data.result, function(i) {
                var result = data.result[i];
                var tr = $('.result').append($('<tr>'));
                tr.append( $('<th>').text(id + i) );
                tr.append( $('<td>').text(result.url) );
                tr.append( $('<td>').text(result.indexed) );
            })
            
        },
        error: function (data) {
            $('.alert').addClass('alert-danger');
            $('.alert').text(data.status + ': ' + data.statusText);
        }
    });
});
</script>